/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 1st, 2019                 	*/
/*   Version :          V01                                 */
/*                                                          */
/* ******************************************************** */

#ifndef NVIC_INTERFACE_H_
#define NVIC_INTERFACE_H_

u8 NVIC_u8SplitPriority(u8 Copy_u8SubPriorityBits);
u8 NVIC_u8EnableInterrupt(u8 Copy_u8InterruptNum);
u8 NVIC_u8DisableInterrupt(u8 Copy_u8InterruptNum);
u8 NVIC_u8SetPendingFlag(u8 Copy_u8InterruptNum);
u8 NVIC_u8ClearPendingFlag(u8 Copy_u8InterruptNum);
u8 NVIC_u8SetPriority(u8 Copy_u8InterruptNum, u8 Copy_u8GroupPriority, u8 Copy_u8SubPriority);


#endif /* NVIC_INTERFACE_H_ */
